import org.forgerock.json.resource.ReadRequest
import org.forgerock.json.JsonValue
import org.forgerock.json.resource.ResourceException
import java.security.SecureRandom
import groovy.json.JsonSlurper

import static Constants.*
import java.util.logging.Logger

class Constants {
  static Logger logger = Logger.getLogger("")

  // BusinessException_24
  static final JsonValue SC_500_OTHER_EXCEPTION = [
    "statusCode": "MFA24",
    "message": "Other Error"
  ]
  // BusinessException_2
  static final JsonValue SC_405_METHOD_NOT_ALLOWED = [
    "statusCode": "MFA2",
    "message": "Method Not allowed"
  ]
  // BusinessException_21
  static final JsonValue SC_404_TITLES_NOT_FOUND = [
    "statusCode": "MFA21",
    "message": "Predefined Roles not available"
  ]
  // BusinessException_22
  static final JsonValue SC_400_REQUIRED_FIELDS_MISSING = [
    "statusCode": "MFA22",
    "message": "Invalid Request. Required field(s) missing or empty"
  ]
}

def requiredFieldsMissing(){
  throw new ResourceException(400, "").setDetail(
    new JsonValue(SC_400_REQUIRED_FIELDS_MISSING)
  )
}

def titlesNotFound(){
  throw new ResourceException(404, "").setDetail(
    new JsonValue(SC_404_TITLES_NOT_FOUND)
  )
}

def otherError(){
  throw new ResourceException(500, "").setDetail(
    new JsonValue(SC_500_OTHER_EXCEPTION)
  )
}

def notAllowed(){
  throw new ResourceException(405, "").setDetail(
    new JsonValue(SC_405_METHOD_NOT_ALLOWED)
  )
}

def success(data){
  return new JsonValue([
    "code": 200,
    "reason": "Success",
    "detail":[
        "statusCode": "MFA1",
        "roleList": data
    ]
  ])
}


if (request instanceof ReadRequest){

  def parameters = request.additionalParameters

  if ( null == parameters || null == parameters["isAdmin"]){
    return requiredFieldsMissing()
  }

  def userType = parameters["isAdmin"].toString()
  if ( !userType.equals("true") && !userType.equals("false") ){
    return titlesNotFound()
  }

  def titleType = "user"
  if ( userType.equals("true") ){
    titleType = "admin"
  }

  def userRepo = null
  try{
    def filterCondition = 'type eq "' + titleType + '"'
    def filter = [ "_queryFilter" : filterCondition ]
    userRepo = openidm.query("managed/mfsDealerRoles", filter)

  }catch(Exception e){
    return otherError()
  }

  if ( null == userRepo || null == userRepo.result[0] )
    return titlesNotFound()

  return success(userRepo.result)

} else {
  return notAllowed()
}
