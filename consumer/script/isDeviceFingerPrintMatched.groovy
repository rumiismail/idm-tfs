import org.forgerock.json.resource.CreateRequest
import org.forgerock.json.JsonValue
import java.security.SecureRandom
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import static Constants.*
import java.util.*
import groovy.json.*

class Constants {
  static final String deviceFingerPrintField = "deviceFP"

  static final String USER_DOES_NOT_EXIST = "User does not exist"
  static final String METHOD_NOT_ALLOWED = "Method Not Allowed"
  static final String OTHER_EXCEPTION = "Other exception"
  static final String FINGERPRINT_NOT_MATCHED = "NOTMATCHED"
  static final String FINGERPRINT_MATCHED = "MATCHED"
  static final String SUCCESS = "success"

  static final boolean base64Encoded = true
}

def notFound(){
  return new JsonValue([
    "code": 404,
    "reason": USER_DOES_NOT_EXIST,
    "message": USER_DOES_NOT_EXIST,
    "data": []
  ])
}

def notAllowed(){
  return new JsonValue([
    "code": 405,
    "reason": METHOD_NOT_ALLOWED,
    "message": METHOD_NOT_ALLOWED,
    "data": []
  ])
}

def otherError(){
  return new JsonValue([
    "code": 500,
    "reason": OTHER_EXCEPTION,
    "message": OTHER_EXCEPTION,
    "data": []
  ])
}

def invalidOtp(){
  return new JsonValue([
    "code": 500,
    "reason": INVALID_OTP_CODE,
    "message": INVALID_OTP_CODE,
    "data": []
  ])
}

def success(data){
  return new JsonValue([
    "code": 200,
    "status": SUCCESS,
    "data": data
  ])
}

def filterOutFields(data){
  data.remove('lastSelectedDate')
  data.devicePrint.remove('geolocation')
  return data
}

def getFingerPrintValue(data){
  try{
    if (base64Encoded == true){
      data = new String(data.decodeBase64())
    }
    data =  new JsonSlurper().parseText(data)
    return filterOutFields(data)

  }catch(Exception e){
    return null
  }
}

if (request instanceof CreateRequest){

  def reqBody = request.content.getObject()

  if ( null == reqBody || null == reqBody["userName"] )
    return otherError()

  if ( null == reqBody || null == reqBody[deviceFingerPrintField] )
    return otherError()

  def userName = (reqBody["userName"]).toString()
  def devFpUser = (reqBody[deviceFingerPrintField]).toString()

  def filter = [ "_queryFilter" : 'userName eq "' + userName + '"' ]
  def userRepo = openidm.query("managed/user", filter)

  if ( null == userRepo || null == userRepo.result[0] )
    return notFound()
 
  def account = userRepo.result[0] 
  String [] devFpRepo = account[deviceFingerPrintField]

  if ( null == devFpRepo || devFpRepo.length == 0)
    return otherError()

  def count = devFpRepo.length
  def matched = false

  def repoFpVal = ""
  def userFpVal = ""

  userFpVal = getFingerPrintValue(devFpUser)

  if (userFpVal != null && userFpVal.length != 0){
    for (String value: devFpRepo){
      repoFpVal = getFingerPrintValue(value)
      if (repoFpVal == null || repoFpVal.length == 0)
        continue

      if (userFpVal == repoFpVal){
        matched = true
        break
      }
    }
  }

  if (matched){
    success(FINGERPRINT_MATCHED)
  }else{
    success(FINGERPRINT_NOT_MATCHED)
  }

} else {
  return notAllowed()
}
	
