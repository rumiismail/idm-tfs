import org.forgerock.json.resource.CreateRequest
import org.forgerock.json.JsonValue
import java.security.SecureRandom

import static Constants.*

class Constants {
  static final String otpField = "otpCode"

  static final String USER_DOES_NOT_EXIST = "User does not exist"
  static final String METHOD_NOT_ALLOWED = "Method Not Allowed"
  static final String OTHER_EXCEPTION = "Other exception"
  static final String INVALID_OTP_CODE = "Invalid OTP Code"
  static final String SUCCESS = "success"
}

def notFound(){
  return new JsonValue([
    "code": 404,
    "reason": USER_DOES_NOT_EXIST,
    "message": USER_DOES_NOT_EXIST,
    "data": []
  ])
}

def notAllowed(){
  return new JsonValue([
    "code": 405,
    "reason": METHOD_NOT_ALLOWED,
    "message": METHOD_NOT_ALLOWED,
    "data": []
  ])
}

def otherError(){
  return new JsonValue([
    "code": 500,
    "reason": OTHER_EXCEPTION,
    "message": OTHER_EXCEPTION,
    "data": []
  ])
}

def success(data){
  return new JsonValue([
    "code": 200,
    "status": SUCCESS,
    "data": data
  ])
}

if (request instanceof CreateRequest){

  def reqBody = request.content.getObject()

  if ( null == reqBody )
    return otherError()

  def filerCondition = null
  def returnMail = false

  if ( null != reqBody["mail"] ){
    def mail = (reqBody["mail"]).toString()
    filterCondition = 'mail eq "' + mail + '"'
  }else if (null != reqBody["userName"]){
    returnMail = true
    def userName = (reqBody["userName"]).toString()
    filterCondition = 'userName eq "' + userName + '"'
  }else{
    otherError()
  }

  def filter = [ "_queryFilter" : filterCondition ]
  def userRepo = openidm.query("managed/user", filter)

  if ( null == userRepo || null == userRepo.result[0] )
    return notFound()
 
  def account = userRepo.result[0] 
  def uid = account["_id"]
  def otpRepo = account[otpField]

  if ( null == otpRepo || otpRepo.trim().isEmpty() )
    return otherError()

  def data = {}

  if (returnMail) {
    data = [
      "token": "value",
      "mail": account["mail"]
    ]
  }else{
    data = [
      "token": "value"
    ]
  }

  return success(data)

} else {
  return notAllowed()
}
	
